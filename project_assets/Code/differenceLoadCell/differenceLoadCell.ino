/**
 *
 * HX711 library for Arduino - example file
 * https://github.com/bogde/HX711
 *
 * MIT License
 * (c) 2018 Bogdan Necula
 *
**/
#include "HX711.h"
#include <Wire.h>
#include <Servo.h>
#include <LiquidCrystal.h> // LiquidCrystal_I2C library
const int rs = 12, en = 11, d4 = 4, d5 = 5, d6 = 6, d7 = 7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

Servo servo;

//HX711 circuit_wiring;
const int LOADCELL_DOUT_PIN = 8;
const int LOADCELL_SCK_PIN = 9;

HX711 scale;

float previousValue = 0;
float moneyAmount = 0;
float objective = 0;
bool flag = false;
bool doorOpened = false;

void setup() {

  previousValue = 0;
  moneyAmount = 0;
  objective = 0;
  flag = false;
  doorOpened = false;
  
  // Servo

  servo.attach(3);
  servo.write(0);

  // Buttons

  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);

  // Leds

  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);

  digitalWrite(A0, LOW);
  digitalWrite(A1, LOW);

  // Turn red led on

  digitalWrite(A1, HIGH);

  lcd.begin(16, 2); // begins connection to the LCD module

  bool selection = false;
  while(selection == false) {
    lcd.setCursor(0,0);
    lcd.print("Amount to save?");
    lcd.setCursor(0,1);
    lcd.print(objective);
    int buttonPlus = digitalRead(A5);
    int buttonMinus = digitalRead(A4);
    int buttonConfirm = digitalRead(A3);
    delay(150);
    if(buttonPlus == 1) {
      if(objective < 10) {
        objective += 0.05;
      }
    }

    if(buttonMinus == 1) {
      if(objective > 0.05) {
        objective -= 0.05;
      }
    }

    if(buttonConfirm == 1) {
      if(objective > 0) {
        selection = true;
      }
    }
  }
  
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  scale.set_scale(100);
  scale.tare();               
  lcd.clear();
}

int buttonDoor;

void loop() {

  buttonDoor = digitalRead(A2);

  if(moneyAmount >= objective){

    digitalWrite(A1, LOW);
    digitalWrite(A0, HIGH);
    flag = true;
  }

  if(buttonDoor == 1) {
    if(flag == true) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Door Opening");

      for(int i = 0; i <= 45; i++){
        servo.write(i);
        delay(100);
      }

      doorOpened = true;
      
    }
  }

  buttonDoor = 0;
  
  while(doorOpened){

    buttonDoor = digitalRead(A2);
    if(buttonDoor == 1){

      digitalWrite(A0, LOW);

      doorOpened = false;
      setup();
      
    }
    
  }

  float valueScale = scale.get_units(20); // get output value
 
  float difference = valueScale - previousValue;
  lcd.setCursor(0, 0); // set cursor to first row
  lcd.print("Coins (Euro):"); // print out to LCD
  if(difference >= 4) {
  
    previousValue = valueScale;

    if(difference > 3.6 && difference < 3.85) {
      moneyAmount += 0.05;
    }
    if(difference > 4 && difference < 5) {
      moneyAmount += 0.10;  
    }

    else if(difference > 5 && difference < 6) {
      moneyAmount += 0.50;  
    }
   
    else if(difference > 7.6 && difference < 8.2) {
      moneyAmount += 0.50;  
    }

    else if(difference > 6 && difference < 7.6) {
      moneyAmount += 1.0;  
    }

    else if(difference > 8 && difference < 9) {
      moneyAmount += 2.0;  
    }

  }

  lcd.setCursor(0, 1);
  lcd.print(moneyAmount);
  
}
